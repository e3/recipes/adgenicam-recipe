# ADGenICam conda recipe

Home: "https://github.com/areaDetector/ADGenICam"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADGenICam module
