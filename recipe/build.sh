#!/bin/bash

# LIBVERSION shall only include MAJOR.MINOR.PATCH for require
LIBVERSION=$(echo ${PKG_VERSION}| cut -d'.' -f1-3)
E3_ADGENICAM_LOCATION="${EPICS_MODULES}/${PKG_NAME}/${LIBVERSION}"

# Clean between variants builds
make clean

make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db_internal
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install
